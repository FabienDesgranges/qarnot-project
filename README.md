# qarnot-project

Pokedex réalisé dans le cadre du test technique de Qarnot.

Affichage des 20 premiers pokemons, fonctions de recherche d'un pokemon par son nom et son type, tri des pokemon par leur nom et leur ID.
## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
